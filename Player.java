import java.util.Set;
import java.util.HashMap;

/**
 * Class Player - a Player in an adventure game.
 *
 * This class is part of the "World of Zuul" application. 
 * "World of Zuul" is a very simple, text based adventure game.  
 *
 * A "Player" represents one location in the scenery of the game.  It is 
 * connected to other Players via exits.  For each existing exit, the Player 
 * stores a reference to the neighboring Player.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */

public class Player 
{
    private String description;
    private Item item; 
    private HashMap<String, Player> exits;
    private HashMap<String, Item> items;
    // stores exits of this Player.

    /**
     * Create a Player described "description". Initially, it has
     * no exits. "description" is something like "a kitchen" or
     * "an open court yard".
     * @param description The Player's description.
     */
    public Player(String description) 
    {
        this.description = description;
        exits = new HashMap<>();
        items = new HashMap<>();
    }

    /**
     * Define an exit from this Player.
     * @param direction The direction of the exit.
     * @param neighbor  The Player to which the exit leads.
     */
    public void setExit(String direction, Player neighbor) 
    {
        exits.put(direction, neighbor);
    }

    /**
     * @return The short description of the Player
     * (the one that was defined in the constructor).
     */
    public String getShortDescription()
    {
        return description;
    }

    /**
     * Return a description of the Player in the form:
     *     You are in the kitchen.
     *     Exits: north west
     * @return A long description of this Player
     */
    public String getLongDescription()
    {
        return " " + description + ".\n" + getExitString();
    }

    /**
     * Puts an item into the room
     */
    public void addItem(Item item)
    {
        items.put(item.getName(), item);
    }

    public Item removeItem(String name)
    {
        return items.remove(name);
    }

    /**
     * Return a string describing the Player's exits, for example
     * "Exits: north west".
     * @return Details of the Player's exits.
     */
    private String getExitString()
    {
        String returnString = "Exits:";
        Set<String> keys = exits.keySet();
        for(String exit : keys) {
            returnString += " " + exit;
        }
        return returnString;
    }

    /**
     * Return the Player that is reached if we go from this Player in direction
     * "direction". If there is no Player in that direction, return null.
     * @param direction The exit's direction.
     * @return The Player in the given direction.
     */
    public Player getExit(String direction) 
    {
        return exits.get(direction);
    }
}

