import java.util.HashMap;
import java.util.Iterator;
/**
 * Write a description of class Items here.
 *
 * @author (Milan Weug)
 * @version (v1.3)
 */
public class Items
{
    private HashMap<String, Item> items;
    /**
     * Constructor for objects of class Items
     */
    public Items()
    {
        items = new HashMap();
    }

    public void put(String name, Item value) {
        items.put(name, value);
    }

    public Item get(String name) {
        return items.get(name);
    }

    public String getItemDescription() {
        String itemDescription = "";
        for(Iterator it = items.values().iterator(); it.hasNext();) {
            itemDescription += " " + ((Item) it.next()).getDescription();
        }
        return itemDescription;
    }

    public String getItemName() {
        String itemName = "";
        for (Iterator it = items.values().iterator(); it.hasNext();) {
            itemName += ((Item) it.next()).getName() + ", ";
        }
        return itemName;
    }

    public Item remove(String name) {
        return (Item) items.remove(name);
    }

}