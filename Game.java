import java.util.HashMap;
/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  Players, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */

public class Game 
{
    private Parser parser;
    private Player currentPlayer;
    private HashMap<String, Player> players;
    private Player1 player1;
    private int turns = 22;

    /**
     * Create the game and initialise its internal map.
     */
    public Game() 
    {
        createPlayers();
        parser = new Parser();
    }
    
    public static void main(String[] arg) 
    {
    Game game = new Game();
    game.play();

    }

    /**
     * Create all the Players and link their exits together.
     */
    private void createPlayers()
    {
        Player  goalKeeper, rightCenterBack, leftCenterBack, rightBack, leftBack, rightCenterMid, leftCenterMid, rightMid, leftMid, leftStriker, rightStriker;

        players = new HashMap<>();
        // create the Players
        goalKeeper = new Player("You are the goalkeeper and you got the ball. You are able to pass it to the right-centerback.");
        rightCenterBack = new Player("Now the right-centerback has got the ball.");
        players.put("rightCenterBack", rightCenterBack);
        leftCenterBack = new Player("Now the ball is at the left-centerback.");
        rightBack = new Player("The rightback accepts the ball in a fabulous way.");
        leftBack = new Player("The leftback has got the ball and is keen on playing the ball foward.");
        players.put("leftBack", leftBack);
        rightCenterMid = new Player("The maestro of the field recieved the ball.");
        leftCenterMid = new Player("Let's see if the left-centermid can do something with this ball.");
        players.put("leftCenterMid", leftCenterMid);
        rightMid = new Player("The quickest player on the field recieves the ball.");
        players.put("rightMid", rightMid);
        leftMid = new Player("Let's see if the left-mid has got something up his sleeve.");
        players.put("leftMid", leftMid);
        leftStriker = new Player("The left-striker has got the ball and he rarely misses the goal.");
        players.put("leftStriker", leftStriker);
        rightStriker = new Player("The ball fell to the right-striker and I think he is going to score.");
        players.put("rightStriker", rightStriker);

        // initialise Player exits
        goalKeeper.setExit("north", rightCenterBack);

        rightCenterBack.setExit("north", rightCenterMid);
        rightCenterBack.setExit("east", rightBack);
        rightCenterBack.setExit("south", goalKeeper);
        rightCenterBack.setExit("west", leftCenterBack);

        leftCenterBack.setExit("north", leftCenterMid);
        leftCenterBack.setExit("east", rightCenterBack);
        leftCenterBack.setExit("west", leftBack);

        rightBack.setExit("north", rightMid);
        rightBack.setExit("west", rightCenterBack);

        leftBack.setExit("north", leftMid);
        leftBack.setExit("east", leftCenterBack);

        rightCenterMid.setExit("north", rightStriker);
        rightCenterMid.setExit("east", rightMid);
        rightCenterMid.setExit("south", rightCenterBack);
        rightCenterMid.setExit("west", leftCenterMid);

        leftCenterMid.setExit("north", leftStriker);
        leftCenterMid.setExit("east",rightCenterMid);
        leftCenterMid.setExit("south", leftCenterBack);
        leftCenterMid.setExit("west", leftMid);

        rightMid.setExit("south", rightBack);
        rightMid.setExit("west", rightCenterMid);
        rightMid.addItem(new Item("tactical report", "the coach mentions that the left-Mid has got a great cross"));

        leftMid.setExit("east", leftCenterMid);
        leftMid.setExit("south", leftBack);

        leftStriker.setExit("east", rightStriker);
        leftStriker.setExit("south", leftCenterMid);

        rightStriker.setExit("south", rightCenterMid);
        rightStriker.setExit("west", leftStriker);

        currentPlayer = goalKeeper;  // start game at the goalkeeper
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    public void play() 
    {            
        printWelcome();

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.

        boolean finished = false;
        while (! finished) {
            Command command = parser.getCommand();
            finished = processCommand(command);
        }
        System.out.println("Thank you for playing.  Good bye.");
    }

    /**
     * Print out the opening message for the player.
     */
    private void printWelcome()
    {
        System.out.println();
        System.out.println("Welcome to the Tiki Taka game!");
        System.out.println("Tiki Taka is a new, incredibly exciting soccer game.");
        System.out.println("Type 'help' if you need help.");
        System.out.println();
        System.out.println(currentPlayer.getLongDescription());
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    private boolean processCommand(Command command) 
    {
        boolean wantToQuit = false;

        CommandWord commandWord = command.getCommandWord();

        switch (commandWord) {
            case UNKNOWN:
            System.out.println("I don't know what you mean..");
            break;

            case HELP:
            printHelp();
            break;

            case PASS:
            goPlayer(command);
            timer();
            break;

            case LOOK:
            look();
            break;

            case SEARCH:
            search();
            break;

            case CROSS:
            cross();
            break;

            case SCORE:
            score();
            break;

            case TAKE:
            take(command);
            break;

            case QUIT:
            wantToQuit = quit(command);
            break;
        }
        return wantToQuit;
    }

    // implementations of user commands:

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the 
     * command words.
     */
    private void printHelp() 
    {
        System.out.println("You're playing a soccer game, you have a few minutes remaining and you need to score the winning goal");
        System.out.println();
        System.out.println("Your command words are:");
        parser.showCommands();
    }

    /** 
     * Try to in to one direction. If there is an exit, enter the new
     * Player, otherwise print an error message.
     */
    private void goPlayer(Command command) 
    {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("Pass where?");
            return;
        }

        String direction = command.getSecondWord();

        // Try to leave current Player.
        Player nextPlayer = currentPlayer.getExit(direction);

        if (nextPlayer == null) {
            System.out.println("There is no one to pass to here!");
        }
        else {
            currentPlayer = nextPlayer;
            System.out.println(currentPlayer.getLongDescription());
        }
    }

    /** 
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    private boolean quit(Command command) 
    {
        if(command.hasSecondWord()) {
            System.out.println("Quit what?");
            return false;
        }
        else {
            return true;  // signal that we want to quit
        }
    }

    private void look()
    {
        System.out.println(currentPlayer.getLongDescription());
    }

    private void take(Command command)
    {
        if(!command.hasSecondWord()) {
            System.out.println("What do you want to pickup?");
            return;
        }

        String itemName = command.getSecondWord();
        Item item = player1.pickUp(itemName);

        if(item == null) {
            System.out.println("There is nothing to pickup");
        }
        else { System.out.println("You picked up:" + item.getName() + " " + item.getDescription());
        }

    }

    private void search()
    {
        if(currentPlayer == players.get("leftBack")) {
            System.out.println("The rightwinger of the opposite team is lurking for an interception..");
        }
        else if(currentPlayer == players.get("rightCenterBack")) {
            System.out.println("The right-centermid isn't really paying attention..");
        }
        else if(currentPlayer == players.get("leftCenterMid")) {
            System.out.println("The left-striker is behind enemy lines, but is he onside?");
        }
        else if(currentPlayer == players.get("leftMid")) {
            System.out.println("You have a lot of space, maybe a cross to the other side of the field would work out.");
        }
        else{
            System.out.println("You are good to go, the players around you are in free-space.");
        }
    }

    private void cross()
    {
        if(currentPlayer == players.get("leftMid")) {
            Player next = players.get("rightStriker");
            currentPlayer = next;
            System.out.println("The ball has been crossed to the other side of the field, to the right-striker");
        }
        else {
            System.out.println("You are not able to cross yet");
        }
    }

    private void score()
    { 
        if(currentPlayer == players.get("leftStriker")) {
            System.out.println("GOOAAAAAAL!!!");
            System.out.println("You have won the World Cup, congratulations!!!");
        }
    }

    private void timer ()
    {
        turns -=1;
        System.out.println("passes left: " + turns);

        if(turns == 11)
        {
            System.out.println("Half way there! You better hurry up.");
        }
        else if(turns <= 0) {
            System.out.println("Time's up! Better luck next time.");
            System.exit(0);
        }
    }
}
