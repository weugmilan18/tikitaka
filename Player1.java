
/**
 * class Player1 - geef hier een beschrijving van deze class
 *
 * @author (jouw naam)
 * @version (versie nummer of datum)
 */
public class Player1
{
    // instance variables - vervang deze door jouw variabelen
    private Items items;
    private Player currentPlayer;

    /**
     * Constructor voor objects van class Player1
     */
    public Player1()
    {
        items = new Items();

    }

    public Item pickUp(String itemName)
    {
        Item item = currentPlayer.removeItem(itemName);
        items.put(item.getName(), item);
        return item;
    }

}
