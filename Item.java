
/**
 * class Item - geef hier een beschrijving van deze class
 *
 * @author (jouw naam)
 * @version (versie nummer of datum)
 */
public class Item
{
    // instance variables - vervang deze door jouw variabelen
    private String name;
    private String description;

    /**
     * Constructor voor objects van class Item
     */
    public Item(String name, String description)
    {
        this.name = name;
        this.description = description;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

}
